package bearman;

import food.Food;

public class Bearman {
	private boolean hungry;
	
	/**
	 * Creates a bearman, and initializes him to hungry
	 */
	public Bearman() {
		hungry = true;
	}

	public boolean isHungry() {
		return hungry;
	}

	public void setHungry(boolean hungry) {
		this.hungry = hungry;
	}
	
	public void eat(Food f) {
		f.feedBearman(this);
	}
	
}
